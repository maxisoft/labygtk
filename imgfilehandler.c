#include <gtk/gtk.h> 
#include "imgfilehandler.h"

void init_all_Pixbuf() {
	GError *imgloaderr = NULL;

	_0000_0 = gdk_pixbuf_new_from_file("./ress/0000_0.png", &imgloaderr);
	_0000_1 = gdk_pixbuf_new_from_file("./ress/0000_1.png", &imgloaderr);
	_0011_0 = gdk_pixbuf_new_from_file("./ress/0011_0.png", &imgloaderr);
	_0011_1 = gdk_pixbuf_new_from_file("./ress/0011_1.png", &imgloaderr);
	_0101_0 = gdk_pixbuf_new_from_file("./ress/0101_0.png", &imgloaderr);
	_0101_1 = gdk_pixbuf_new_from_file("./ress/0101_1.png", &imgloaderr);
	_0110_0 = gdk_pixbuf_new_from_file("./ress/0110_0.png", &imgloaderr);
	_0110_1 = gdk_pixbuf_new_from_file("./ress/0110_1.png", &imgloaderr);
	_1001_0 = gdk_pixbuf_new_from_file("./ress/1001_0.png", &imgloaderr);
	_1001_1 = gdk_pixbuf_new_from_file("./ress/1001_1.png", &imgloaderr);
	_1010_0 = gdk_pixbuf_new_from_file("./ress/1010_0.png", &imgloaderr);
	_1010_1 = gdk_pixbuf_new_from_file("./ress/1010_1.png", &imgloaderr);
	_1100_0 = gdk_pixbuf_new_from_file("./ress/1100_0.png", &imgloaderr);
	_1100_1 = gdk_pixbuf_new_from_file("./ress/1100_1.png", &imgloaderr);
	_1111_0 = gdk_pixbuf_new_from_file("./ress/1111_0.png", &imgloaderr);
	_1111_1 = gdk_pixbuf_new_from_file("./ress/1111_1.png", &imgloaderr);
	_blank = gdk_pixbuf_new_from_file("./ress/blank.png", &imgloaderr);
	_bot = gdk_pixbuf_new_from_file("./ress/bot.png", &imgloaderr);
	_test = gdk_pixbuf_new_from_file("./ress/test.png", &imgloaderr);
	_top = gdk_pixbuf_new_from_file("./ress/top.png", &imgloaderr);
}

GdkPixbuf * string_to_Pixbuf(const char* piece_str) {

	if (strcmp("0000_0", piece_str) == 0) {
		return _0000_0;
	}
	else if (strcmp("0000_1", piece_str) == 0) {
		return _0000_1;
	}
	else if (strcmp("0011_0", piece_str) == 0) {
		return _0011_0;
	}
	else if (strcmp("0011_1", piece_str) == 0) {
		return _0011_1;
	}
	else if (strcmp("0101_0", piece_str) == 0) {
		return _0101_0;
	}
	else if (strcmp("0101_1", piece_str) == 0) {
		return _0101_1;
	}
	else if (strcmp("0110_0", piece_str) == 0) {
		return _0110_0;
	}
	else if (strcmp("0110_1", piece_str) == 0) {
		return _0110_1;
	}
	else if (strcmp("1001_0", piece_str) == 0) {
		return _1001_0;
	}
	else if (strcmp("1001_1", piece_str) == 0) {
		return _1001_1;
	}
	else if (strcmp("1010_0", piece_str) == 0) {
		return _1010_0;
	}
	else if (strcmp("1010_1", piece_str) == 0) {
		return _1010_1;
	}
	else if (strcmp("1100_0", piece_str) == 0) {
		return _1100_0;
	}
	else if (strcmp("1100_1", piece_str) == 0) {
		return _1100_1;
	}
	else if (strcmp("1111_0", piece_str) == 0) {
		return _1111_0;
	}
	else if (strcmp("1111_1", piece_str) == 0) {
		return _1111_1;
	}
	else if (strcmp("blank", piece_str) == 0) {
		return _blank;
	}
	else if (strcmp("bot", piece_str) == 0) {
		return _bot;
	}
	else if (strcmp("test", piece_str) == 0) {
		return _test;
	}
	else if (strcmp("top", piece_str) == 0) {
		return _top;
	}
	return NULL;
}