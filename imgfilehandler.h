/* 
 * File:   imgfilehandler.h
 * Author: i5
 *
 * Created on 27 mars 2013, 08:43
 */

#ifndef IMGFILEHANDLER_H
#define	IMGFILEHANDLER_H

GdkPixbuf *_0000_0;
GdkPixbuf *_0000_1;
GdkPixbuf *_0011_0;
GdkPixbuf *_0011_1;
GdkPixbuf *_0101_0;
GdkPixbuf *_0101_1;
GdkPixbuf *_0110_0;
GdkPixbuf *_0110_1;
GdkPixbuf *_1001_0;
GdkPixbuf *_1001_1;
GdkPixbuf *_1010_0;
GdkPixbuf *_1010_1;
GdkPixbuf *_1100_0;
GdkPixbuf *_1100_1;
GdkPixbuf *_1111_0;
GdkPixbuf *_1111_1;
GdkPixbuf *_blank;
GdkPixbuf *_bot;
GdkPixbuf *_test;
GdkPixbuf *_top;

void init_all_Pixbuf();
GdkPixbuf * string_to_Pixbuf(const char* piece_str);

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* IMGFILEHANDLER_H */

